/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package cmdline

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/canvas"
	"os"
)

func CommandQuizzesList(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	course := ctx.Course()
	if err != nil {
		return cli.Exit(err, 1)
	}
	fmt.Println("Quizzes in course:", course.AsName())
	if err := ctx.ListQuizzes(); err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}

func CommandCreateQuizStdin(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	fmt.Println("Creating quiz from STDIN")
	if err = ctx.CreateQuizFromReader(os.Stdin); err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}
