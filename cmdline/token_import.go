/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package cmdline

import (
	"bytes"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/config"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"strings"
	"syscall"
)

func ImportToken(ctx *cli.Context) error {
	if ctx.NArg() < 1 {
		return cli.Exit("Not enough arguments. I need a URL to match this token to.", 1)
	}
	url := strings.TrimSpace(ctx.Args().First())
	conf, err := config.LoadConfig()
	if err != nil {
		return cli.Exit(err, 1)
	}
	var token string = ""
	if site, err := conf.SiteByUrl(url); err == nil {
		if site.Token != "" {
			fmt.Println("[+] Site has a token. I will wrap that token. You canvas.conf will NOT be modified.")
			token = site.Token
		}
	}
	if token == "" {
		fmt.Print("[+] Please provide the token: ")
		tokenBytes, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return err
		}
		fmt.Println()
		token = strings.TrimSpace(string(tokenBytes))
	}
	fmt.Print("Please provide a password: ")
	pwdBytes, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return err
	}
	pwdBytes = bytes.TrimSpace(pwdBytes)
	fmt.Print("\nPlease repeat your password: ")
	pwdBytes2, err := terminal.ReadPassword(int(syscall.Stdin))
	fmt.Println()
	if err != nil {
		return err
	}
	pwdBytes2 = bytes.TrimSpace(pwdBytes2)

	if len(pwdBytes) != len(pwdBytes2) {
		fmt.Fprintf(os.Stderr, "Error. Passwords do not match")
		return cli.Exit("Cannot import token", 1)
	}
	for i := range pwdBytes2 {
		if pwdBytes[i] != pwdBytes2[i] {
			fmt.Fprintf(os.Stderr, "Error. Passwords do not match")
			return cli.Exit("Cannot import token", 1)
		}
	}

	ciphertext, err := config.EncryptToken(token, url, pwdBytes)

	fmt.Println("Place this in your canvas.conf file:")
	fmt.Printf("Token = \"%s\"\n", config.EncodeToken(ciphertext.Ciphertext, ciphertext.Nonce, ciphertext.Salt))
	return nil
}
