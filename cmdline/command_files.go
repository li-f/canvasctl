/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package cmdline

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/canvas"
	"gitlab.com/mgelde/canvasctl/rest"
	"os"
	"path/filepath"
)

func CommandFilesList(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	if err := ctx.ListFiles(); err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}

func CommandFilePush(c *cli.Context) error {
	var ret error = nil
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	if c.NArg() <= 0 {
		fmt.Println("Usage:", c.Command.Name, c.Command.ArgsUsage)
		return cli.Exit("Not enough arguments.", 1)
	}

	var moduleId rest.CanvasId = 0
	haveModule := false
	if c.IsSet("module") {
		name := c.String("module")
		if moduleId, err = ctx.ModuleIdFromName(name, true); err != nil {
			moduleId = 0
			logrus.Error(err)
			fmt.Fprintf(os.Stderr, "Cannot find or create module '%s'. Reason: %s\n", name, err)
		} else {
			haveModule = true
			logrus.Infof("Have module with id %d", moduleId)
		}
	}
	var maxLength int = 0
	for i := 0; i < c.NArg(); i++ {
		if len(c.Args().Get(i)) > maxLength {
			maxLength = len(c.Args().Get(i))
		}
	}
	fmt.Println("Pushing file:")
	for i := 0; i < c.NArg(); i++ {
		localname := c.Args().Get(i)
		remotename := filepath.Base(localname)
		remotepath := c.String("path")

		fmt.Printf(" - %-*s   ", maxLength, localname)
		logrus.Infof("localname=%s, remotename=%s, path=%s", localname, remotename, remotepath)
		fileId, url, err := ctx.PushFile(localname, remotename, remotepath)
		if err != nil {
			fmt.Println("[FAILED]")
			ret = cli.Exit(err, 1)
			continue
		} else {
			fmt.Println("[OK]")
			if c.IsSet("details") {
				fmt.Println("   ", url)
			}
		}

		logrus.Debug("File has id:", fileId)
		if haveModule {
			if item, ok := ctx.FindModuleItem(moduleId, func(m rest.ModuleItem) bool {
				if m.ContentId == fileId {
					return true
				} else {
					return false
				}
			}); ok {
				logrus.Infof("Found a module item with file ID %d, namely item ID %d", fileId, item.Id)
				break
			}
			if _, err = ctx.AddModuleItem(moduleId, fileId, remotename, canvas.MODITEM_FILE); err != nil {
				ret = cli.Exit(err, 1)
				fmt.Fprintln(os.Stderr, "Could not add file to module:", err)
			}
		}
		if !c.Bool("publish") {
			logrus.Info("Unpublishing file...")
			err := ctx.PublishFile(fileId, false)
			logrus.Debugf("Done unplublishing. Error code: %v", err)
			if err != nil {
				logrus.Error(err)
				ret = cli.Exit(err, 1)
				fmt.Fprintln(os.Stderr, "Error unpublishing file")
			}
		}
	}
	return ret
}
