/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
	"io/ioutil"
	"os"
	"testing"
)

func TestPushFileDoesNotCallApiOnError(t *testing.T) {
	mockAPI := new(MockAPI)

	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})
	_, _, err := context.PushFile("illegalfilename", "remote", "path")
	require.NotNil(t, err, "file does not exist -> should give an errror")

	_, _, err = context.PushFile(os.TempDir(), "remote", "path")
	require.NotNil(t, err, "file is directory -> should give an errror")
	mockAPI.AssertNotCalled(t, "PushFile", mock.Anything, mock.Anything)
	mockAPI.AssertExpectations(t)
}

func TestPushFile(t *testing.T) {
	mockAPI := new(MockAPI)
	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})

	tempfile, _ := ioutil.TempFile(os.TempDir(), "blaPattern")
	tempfile.WriteString("13 bytes long")
	tempfile.Close()

	mockAPI.On("PushFile", rest.CanvasId(1337),
		mock.MatchedBy(func(spec rest.PushFileSpec) bool {
			return spec.FileSize == uint64(13) &&
				spec.RemoteFilename == "remote" &&
				spec.RemotePath == "path"
		})).Return(
		rest.FileUploadResponse{
			Id:          rest.CanvasId(12),
			Size:        123,
			DisplayName: "bla",
			Url:         "https://example.com/bla/foo",
		},
		nil).Once()

	id, url, err := context.PushFile(tempfile.Name(), "remote", "path")
	require.Nil(t, err, "did not expect an error pushing", err)
	require.Equal(t, id, rest.CanvasId(12))
	require.Equal(t, url, "https://example.com/bla/foo")

	mockAPI.On("PushFile", mock.Anything, mock.Anything).Return(
		rest.FileUploadResponse{}, rest.NewAuthError(true)).Once()
	_, _, err = context.PushFile(tempfile.Name(), "ignored", "ignored")
	require.NotNil(t, err, "not authenticated -> should give an errror")
	require.Equal(t, err.Error(), "Error. Not authenticated. Is your token valid?")
	mockAPI.AssertExpectations(t)
}

func TestListFiles(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ListFiles", rest.CanvasId(1337)).Return([]rest.FileInfo{
		{Id: rest.CanvasId(12),
			Filename: "bla1",
			Size:     666,
		},
		{
			Id:       rest.CanvasId(13),
			Filename: "bla2",
			Size:     777,
		},
	}, nil).Once()

	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	err := context.ListFiles()
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}
