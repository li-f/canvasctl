/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package quiz_parsing

import (
	"bytes"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestRoundTrip(t *testing.T) {
	reader := bytes.NewReader([]byte(SampleTomlFile))
	spec, err := QuizSpecFromReader(reader)
	if err != nil {
		t.Fatal("Could not parse TOML:", err)
	}

	var nilStringPtr *string
	var nilBoolPtr *bool
	nilStringPtr = nil
	nilBoolPtr = nil

	require.Equal(t, *spec.Title, "My Quiz")
	require.Equal(t, *spec.Text, "This is a quiz")
	require.Equal(t, *spec.ScoringPolicy, "keep_highest")
	require.Equal(t, *spec.TimeLimit, 90)
	require.Equal(t, *spec.AllowedAttempts, 2)
	require.Equal(t, *spec.ShowCorrectAnswers, true)
	require.Equal(t, spec.HideResults, nilStringPtr)
	require.Equal(t, spec.ShowCorrectAnswersAt, nilStringPtr)
	require.Equal(t, spec.HideCorrectAnswersAt, nilStringPtr)
	require.Equal(t, spec.AccessCode, nilStringPtr)
	require.Equal(t, spec.IPFilter, nilStringPtr)
	require.Equal(t, spec.DueAt, nilStringPtr)
	require.Equal(t, spec.LockAt, nilStringPtr)
	require.Equal(t, spec.UnlockAt, nilStringPtr)
	require.Equal(t, spec.ShuffleAnswers, nilBoolPtr)
	require.Equal(t, spec.ShowCorrectAnswersLastAttempt, nilBoolPtr)
	require.Equal(t, spec.OneQuestionAtATime, nilBoolPtr)
	require.Equal(t, spec.CantGoBack, nilBoolPtr)
	require.Equal(t, spec.Published, nilBoolPtr)
	require.Equal(t, spec.OneTimeResults, nilBoolPtr)
	require.Equal(t, len(spec.Groups), 1)
	group := spec.Groups[0]
	require.Equal(t, group.Name, "Group 1")
	require.Equal(t, group.PickCount, 1)
	require.Equal(t, group.Points, 1.2)
	require.Equal(t, len(group.McQuestions), 2)
	require.Equal(t, len(group.MultiAnswerQuestions), 1)
	require.Equal(t, len(group.ShortAnswerQuestions), 1)
	require.Equal(t, len(group.FillBlanksQuestions), 1)

	require.Equal(t, group.McQuestions[0].Correct, "this one is correct")
	require.Equal(t, group.McQuestions[1].Correct, "1337")
	require.Equal(t, group.McQuestions[1].Wrong[3], "1338")
	require.Equal(t, group.McQuestions[1].Title, "Question 2")

	require.Equal(t, group.MultiAnswerQuestions[0].Correct[0], "correct")
	require.Equal(t, group.MultiAnswerQuestions[0].Wrong[1], "are")
	require.Equal(t, group.MultiAnswerQuestions[0].Correct[1], "also correct")

	require.Equal(t, group.ShortAnswerQuestions[0].Answers[0], "one")
	require.Equal(t, group.ShortAnswerQuestions[0].Answers[1], "two")
	require.Equal(t, group.ShortAnswerQuestions[0].Answers[2], "three")

	require.Equal(t, group.FillBlanksQuestions[0].Answers["a"][0], "question")
	require.Equal(t, group.FillBlanksQuestions[0].Answers["b"][0], "three")
	require.Equal(t, group.FillBlanksQuestions[0].Answers["c"][0], "blank fields")
	require.Equal(t, group.FillBlanksQuestions[0].Answers["c"][1], "blanks")
}

func TestGetFilesFromDescription(t *testing.T) {
	context := GetFilesFromDescription(`
This is a text {%mime=(image/png),filename=(./test.png),type-specific=(x=(123),y=(345))%}
that contains three {%mime=(text/txt),filename=(./bar.txt),folder=(/remotedir)%} markers.
Another one: {%mime=(image/png),filename=(./foo.png),folder=(/folder2),type-specific=(x=(456),y=(666))%}
`)
	require.Len(t, context.FileSpec, 3)

	require.Equal(t, context.FileSpec[0].Filename, "./test.png")
	require.NotNil(t, context.FileSpec[0].TypeSpecific)
	imageSpec := context.FileSpec[0].TypeSpecific.(ImageSpecificData)
	require.Equal(t, "/", context.FileSpec[0].Dirname)
	require.Equal(t, imageSpec.DimX, "123")
	require.Equal(t, imageSpec.DimY, "345")

	require.Equal(t, context.FileSpec[1].Filename, "./bar.txt")
	require.Equal(t, context.FileSpec[1].Dirname, "/remotedir")
	require.Nil(t, context.FileSpec[1].TypeSpecific)

	require.Equal(t, context.FileSpec[2].Filename, "./foo.png")
	require.NotNil(t, context.FileSpec[2].TypeSpecific)
	imageSpec = context.FileSpec[2].TypeSpecific.(ImageSpecificData)
	require.Equal(t, "/folder2", context.FileSpec[2].Dirname)
	require.Equal(t, imageSpec.DimX, "456")
	require.Equal(t, imageSpec.DimY, "666")
}

func TestReplaceFile(t *testing.T) {
	text := `
This is a text {%mime=(image/png),filename=(./test.png),type-specific=(x=(123),y=(345))%} that contains two {%mime=(text/txt),filename=(./bar.txt)%}
markers.`
	context := GetFilesFromDescription(text)
	context.AddReplacement(context.FileSpec[0], "REPLACEMENT")
	context.AddReplacement(context.FileSpec[1], "ANOTHERONE")
	afterReplacement := context.ReplaceFilesInText(text)
	require.Equal(t, afterReplacement, `
This is a text REPLACEMENT that contains two ANOTHERONE
markers.`)
}
