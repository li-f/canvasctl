/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
	"os"
)

func (ctx *CourseContext) ListFiles() error {
	if ctx == nil {
		panic("Context in ListFiles() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	files, err := ctx.API.ListFiles(id)
	if err != nil {
		return err
	}
	fmt.Println("Files in course:", course.AsName())
	for _, file := range files {
		fmt.Println(" -", file.DisplayName)
		fmt.Println("   Hidden:", file.Hidden)
		fmt.Println("   URL:", file.Url)
	}
	return nil
}

func (ctx *CourseContext) PushFile(localname, remotename, remotepath string) (rest.CanvasId, string, error) {
	if ctx == nil {
		panic("Context in PushFile() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		logrus.Error(err)
		return 0, "", fmt.Errorf("Did not get an ID for the course.")
	}
	finfo, err := os.Stat(localname)
	if err != nil {
		if os.IsNotExist(err) {
			return 0, "", fmt.Errorf("File '%s' does not exist.", localname)
		}
		return 0, "", fmt.Errorf("Cannot stat file. %s", err)
	}
	if finfo.Size() < 0 {
		logrus.Errorf("File size was negative: %d", finfo.Size())
		return 0, "", fmt.Errorf("Error during stat.")
	}
	fsize := uint64(finfo.Size())
	if finfo.IsDir() {
		return 0, "", fmt.Errorf("Error. %s is a directory. Please specify a file", localname)
	}
	file, err := os.Open(localname)
	if err != nil {
		logrus.Error(err)
		if os.IsPermission(err) {
			return 0, "", fmt.Errorf("Cannot open file. Permission denied")
		}
		return 0, "", fmt.Errorf("Cannot open file: %s", err)
	}
	defer file.Close()

	logrus.Debugf("calling PushFile. local=%s, remote=%s, remotepath=%s, fsize=%d, id=%d",
		localname, remotename, remotepath, fsize, id)

	pushSpec := rest.PushFileSpec{
		File:           file,
		FileSize:       fsize,
		RemoteFilename: remotename,
		RemotePath:     remotepath,
	}
	uploadResponse, err := ctx.API.PushFile(id, pushSpec)
	if err != nil && rest.NeedsAuthentication(err) {
		return 0, "", fmt.Errorf("Error. Not authenticated. Is your token valid?")
	}
	if err == nil {
		logrus.Debugf("New file has id %d", uploadResponse.Id)
	}
	return uploadResponse.Id, uploadResponse.Url, err
}

func (ctx *Context) PublishFile(id rest.CanvasId, publish bool) error {
	if ctx == nil {
		panic("Context in ModifyFile() is nil")
	}
	logrus.Debugf("Attempting to set publication status of file %d to %v", id, publish)

	mod := rest.MakeFileModification().Published(publish)
	_, err := ctx.API.ModifyFile(id, mod)
	if err != nil && rest.NeedsAuthentication(err) {
		return fmt.Errorf("Error. Not authenticated. Is your token valid?")
	} else if err != nil {
		return fmt.Errorf("Error modifying file %s", err)
	}
	logrus.Debug("Success")
	return nil
}
