/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
)

type CanvasCourse interface {
	AsName() string
	AsId() (rest.CanvasId, error)
}

type integerCourseId struct {
	Id   rest.CanvasId
	API  rest.API
	name string
}

func (i integerCourseId) AsId() (rest.CanvasId, error) {
	return i.Id, nil
}

const UnknownCourse = "UNKNOWN"

func (i integerCourseId) AsName() string {
	if i.name == "" {
		id, _ := i.AsId() // cannot fail
		course, err := i.API.GetSingleCourse(id)
		if err != nil {
			if rest.NeedsAuthentication(err) {
				return UnknownCourse
			}
			logrus.Error(err)
			return UnknownCourse
		}
		i.name = course.Name
	}
	return i.name
}

type stringCourseId struct {
	Name string
	API  rest.API
}

func (i stringCourseId) AsName() string {
	return i.Name
}

func (i stringCourseId) AsId() (rest.CanvasId, error) {
	logrus.Info("stringCourseId. Have no integer ID yet. Performing lookup.")
	courses, err := i.API.GetCourses()
	if err != nil {
		if rest.NeedsAuthentication(err) {
			return 0, fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		return 0, err
	}
	for _, course := range courses {
		if course.Name == i.Name {
			logrus.Debugf("Success... Found course with id %d", course.Id)
			return course.Id, nil
		}
	}
	return 0, fmt.Errorf("No course with name %s found.", i.Name)
}

func CourseFromId(api rest.API, id rest.CanvasId) CanvasCourse {
	return &integerCourseId{
		Id:  id,
		API: api,
	}
}

func CourseFromName(api rest.API, name string) CanvasCourse {
	return &stringCourseId{
		Name: name,
		API:  api,
	}
}

func (ctx *Context) ListCourses() error {
	if ctx == nil {
		panic("Context in ListCourses() is nil")
	}
	courses, err := ctx.API.GetCourses()
	if err != nil {
		if rest.NeedsAuthentication(err) {
			return fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		return err
	}
	for _, c := range courses {
		fmt.Println(c)
	}
	return nil
}

func (ctx *CourseContext) ShowCourse() error {
	if ctx == nil {
		panic("Context in ShowCourse() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	courseDetail, err := ctx.API.GetSingleCourse(id)
	if err != nil {
		if rest.NeedsAuthentication(err) {
			return fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		logrus.Error("Error getting single course:", err)
		return err
	}
	fmt.Println("Course:\n--------")
	fmt.Println("- Name:", courseDetail.Name)
	fmt.Println("- Id:", courseDetail.Id)
	fmt.Println("- Code:", courseDetail.Code)
	fmt.Println("- Started:", courseDetail.StartedAt.Local())
	return nil
}
