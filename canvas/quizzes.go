/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/canvas/quiz_parsing"
	"gitlab.com/mgelde/canvasctl/rest"
	"gitlab.com/mgelde/canvasctl/rest/quizzes"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func (ctx *CourseContext) ListQuizzes() error {
	if ctx == nil {
		panic("Context in ListFiles() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	quizzes, err := ctx.API.ListQuizzes(id)
	if err != nil {
		return err
	}
	for _, quiz := range quizzes {
		fmt.Println(" -", quiz)
	}
	return nil
}

func (ctx *CourseContext) createQuestion(question quiz_parsing.QuestionSpec, canvasGroupId rest.CanvasId) (quizzes.QuestionBuilder, error) {
	filenames := quiz_parsing.GetFilesFromDescription(question.Text)
	var err error = nil
	courseId, err := ctx.course.AsId()
	if err != nil {
		return nil, err
	}
	for _, embeddedFileSpec := range filenames.FileSpec {
		filename := embeddedFileSpec.Filename
		remotename := filepath.Base(filename)
		dirname := embeddedFileSpec.Dirname
		fileId, _, err := ctx.PushFile(filename, remotename, dirname)
		if err != nil {
			logrus.Errorf("Could not push file %s to directory %s", filename, dirname)
			filenames.AddReplacement(embeddedFileSpec, "ERROR DURING UPLOAD")
			continue
		}
		if strings.HasPrefix(embeddedFileSpec.MimeType, "image/") {
			if embeddedFileSpec.TypeSpecific != nil {
				imgSpecificSpec := embeddedFileSpec.TypeSpecific.(quiz_parsing.ImageSpecificData)
				filenames.AddReplacement(embeddedFileSpec, fmt.Sprintf(
					`<img src="/courses/%s/files/%s/preview" alt="%s" width="%s" height="%s" />`,
					courseId.ToBase10String(), fileId.ToBase10String(), remotename,
					imgSpecificSpec.DimX,
					imgSpecificSpec.DimY))
			} else {
				filenames.AddReplacement(embeddedFileSpec, fmt.Sprintf(
					`<img src="/courses/%s/files/%s/preview" alt="%s"/>`,
					courseId.ToBase10String(), fileId.ToBase10String(), remotename))
			}
		} else {
			filenames.AddReplacement(embeddedFileSpec, fmt.Sprintf(
				`<a class="instructure_file_link" title="%s" href="/courses/%s/files/%s/download?wrap=1">%s</a>`,
				remotename, courseId.ToBase10String(), fileId.ToBase10String(), remotename))
		}
	}
	modified_text := filenames.ReplaceFilesInText(question.Text)
	qParams := quizzes.NewQuestion().
		SetTitle(question.Title).
		SetQuizGroup(canvasGroupId).
		AddDescription(modified_text)
	return qParams, err
}

func createMcQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.McQuestionSpec) error {
	answerBuilder := qParams.MakeMultipleChoiceQuestion()
	answerBuilder.AddAnswer(strings.TrimSpace(question.Correct), true)
	for _, falseAnswer := range question.Wrong {
		answerBuilder.AddAnswer(strings.TrimSpace(falseAnswer), false)
	}
	logrus.Infof("Creating question: %s (multiple choice)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createMultiAnswerQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.MultAnswersQuestionSpec) error {
	answerBuilder := qParams.MakeMultipleAnswerQuestion()
	for _, correctAnswer := range question.Correct {
		answerBuilder.AddAnswer(strings.TrimSpace(correctAnswer), true)
	}
	for _, falseAnswer := range question.Wrong {
		answerBuilder.AddAnswer(strings.TrimSpace(falseAnswer), false)
	}
	logrus.Infof("Creating question: %s (multiple answers)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createNumericQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.NumericQuestionSpec) error {
	for _, approximateAnswer := range question.Approximate {
		qParams.MakeNumericAnswer().AddAnswerApproximate(
			approximateAnswer.Answer, approximateAnswer.Precision)
	}
	for _, exactAnswer := range question.Exact {
		qParams.MakeNumericAnswer().AddAnswerExact(
			exactAnswer.Answer, exactAnswer.ErrorMargin)
	}
	for _, rangeAnswer := range question.Range {
		qParams.MakeNumericAnswer().AddAnswerRange(
			rangeAnswer.Begin, rangeAnswer.End)
	}
	logrus.Infof("Creating question: %s (numeric answers)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createShortAnswerQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.ShortAnswerQuestionSpec) error {
	builder := qParams.MakeShortAnswer()
	for _, answer := range question.Answers {
		builder.AddAnswer(strings.TrimSpace(answer))
	}
	logrus.Infof("Creating question: %s (short answer)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createFillBlankQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.FillBlanksQuestionSpec) error {
	builder := qParams.MakeMultipleBlankQuestion()
	for field, answerList := range question.Answers {
		for _, answer := range answerList {
			builder.AddAnswer(strings.TrimSpace(field), strings.TrimSpace(answer))
		}
	}
	logrus.Infof("Creating question: %s (fill multiple blanks)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	params, err := builder.ValidateAndReturn()
	if err != nil {
		return err
	}
	_, err = api.CreateQuestion(courseId, quizId, params)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func (ctx *CourseContext) CreateQuizFromReader(reader io.Reader) error {
	if ctx == nil {
		panic("Context in CreateQuizFromReader is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	quizSpec, err := quiz_parsing.QuizSpecFromReader(reader)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[!] Error. Cannot parse provided TOML file.")
		return err
	}

	fmt.Println("[+] Creating quiz:", quizSpec.Title)
	quiz, err := ctx.API.CreateQuiz(
		id,
		quizSpec.Title,
		quizSpec.Kind,
		quizSpec.Text,
		quizSpec.HideResults,
		quizSpec.ShowCorrectAnswersAt,
		quizSpec.HideCorrectAnswersAt,
		quizSpec.ScoringPolicy,
		quizSpec.AccessCode,
		quizSpec.IPFilter,
		quizSpec.DueAt,
		quizSpec.LockAt,
		quizSpec.UnlockAt,
		quizSpec.TimeLimit,
		quizSpec.AllowedAttempts,
		quizSpec.ShuffleAnswers,
		quizSpec.ShowCorrectAnswers,
		quizSpec.ShowCorrectAnswersLastAttempt,
		quizSpec.OneQuestionAtATime,
		quizSpec.CantGoBack,
		quizSpec.Published,
		quizSpec.OneTimeResults)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Could not create quiz!")
		return err
	}
	for _, groupSpec := range quizSpec.Groups {
		fmt.Println("[+] Creating group:", groupSpec.Name)
		canvasGroup, err := ctx.API.CreateQuizQuestionGroup(id, quiz.Id, groupSpec.Name,
			groupSpec.PickCount, groupSpec.Points)
		if err != nil {
			fmt.Fprintln(os.Stderr, "[!] Could not create question group:", groupSpec.Name)
			fmt.Fprintln(os.Stderr, "    Reason:", err)
			continue
		}
		for _, question := range groupSpec.McQuestions {
			fmt.Println(" -> Creating MC question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createMcQuestion(qParams, ctx.API, id, quiz.Id, question)
		}

		for _, question := range groupSpec.MultiAnswerQuestions {
			fmt.Println(" -> Creating multi-answer question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createMultiAnswerQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.NumericQuestions {
			fmt.Println(" -> Creating numeric question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createNumericQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.ShortAnswerQuestions {
			fmt.Println(" -> Creating short answer question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createShortAnswerQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.FillBlanksQuestions {
			fmt.Println(" -> Creating fill blank question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createFillBlankQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
	}
	if err != nil {
		return fmt.Errorf("There were errors. Please check the result on Canvas matches your exepctations.")
	}
	return nil
}
