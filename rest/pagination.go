/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"container/list"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

func nextPage(header *http.Header) (bool, string) {
	linkHeader := header.Get("Link")
	if linkHeader == "" {
		return false, ""
	}
	logrus.Info("Pagination active. Finding next page...")
	components := strings.Split(linkHeader, ",")
	for _, linkObj := range components {
		parts := strings.Split(linkObj, ";")
		if len(parts) != 2 {
			logrus.Warn("Received unexpected format in Link header: ", linkObj)
			continue
		}
		link := strings.Trim(parts[0], "<> ")
		rel := strings.TrimSpace(parts[1])
		logrus.Debugf("-> Link (%s): %s", rel, link)
		if rel == "rel=\"next\"" {
			logrus.Debug("Success")
			return true, link
		}
	}
	return false, ""
}

func (c *ApiImpl) loadAllPages(req *http.Request) (*list.List, error) {
	var header http.Header
	responsebytes, err := c.processRequestCheckRaw(req, nil, &header)
	if err != nil {
		logrus.Error("Could not perform request for pagination", err)
	}
	responseList := list.New()
	responseList.PushBack(responsebytes)
	for hasNext, url := nextPage(&header); hasNext; hasNext, url = nextPage(&header) {
		req, err := c.assembleRequest("GET", url, nil)
		if err != nil {
			logrus.Error("Could not assemble request for pagination", err)
			break
		}
		responsebytes, err = c.processRequestCheckRaw(req, nil, &header)
		if err != nil {
			logrus.Error("Could not perform request for pagination", err)
			break
		}
		responseList.PushBack(responsebytes)
	}
	return responseList, nil
}
