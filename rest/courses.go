/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"net/url"
)

func (c *ApiImpl) GetSingleCourse(id CanvasId) (Course, error) {
	if c == nil {
		return Course{}, fmt.Errorf("No API")
	}
	req, err := c.assembleRequest("GET", c.makeUri("courses", id.ToBase10String()).String(), nil)
	if err != nil {
		logrus.Error("Cannot assemble request", err)
		return Course{}, err
	}
	var course Course
	err = c.processRequest(req, &course)
	if err != nil {
		return Course{}, err
	}
	return course, nil
}

func (c *ApiImpl) GetCourses() ([]Course, error) {
	if c == nil {
		return nil, fmt.Errorf("No API")
	}
	req, err := c.assembleRequest("GET",
		c.makeUri("courses").SetQuery(
			&url.Values{
				"per_page": {"10"},
			}).String(),
		nil)
	if err != nil {
		return nil, err
	}
	list, err := c.loadAllPages(req)
	if err != nil {
		return nil, err
	}
	array := make([]Course, 0, 10*list.Len())
	for elem := list.Front(); elem != nil; elem = elem.Next() {
		courses, err := unmarshallCourses(elem.Value.([]byte))
		if err != nil {
			logrus.Error("Could not unmarshall bytes in list element. Moving on.")
			continue
		}
		array = append(array, courses...)
	}
	return array, nil
}
