/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"net/url"
	"regexp"
	"testing"
)

func TestListQuizz(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes`)
			require.Equal(t, exp.FindStringSubmatch(req.URL.Path)[1], "666")
			w.Write([]byte(`[{"id":1234, "quiz_type":"practice_quiz"}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if quizzes, err := api.ListQuizzes(666); err != nil {
		t.Fatal("Error listing quizzes", err)
	} else {
		require.Equal(t, len(quizzes), 1)
		require.Equal(t, quizzes[0].Id, CanvasId(1234))
		require.Equal(t, quizzes[0].QuizType, "practice_quiz")
	}
}

func TestAddQuestion(t *testing.T) {
	params := url.Values{
		"bllli":    []string{"a", "b", "c"},
		"ajkadjas": []string{"a"},
	}
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.Method, "POST")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes/(\d+)`)
			match := exp.FindStringSubmatch(req.URL.Path)
			require.Equal(t, match[1], "666")
			require.Equal(t, match[2], "777")
			require.Nil(t, req.ParseForm(), "cannot parse form")
			require.Equal(t, req.PostForm, params)

			w.Write([]byte(`{"id":14102,"quiz_id":1652,"quiz_group_id":638,"assessment_question_id":14008,"position":null,"question_name":"Floop","question_type":"essay_question","question_text":"cyber","points_possible":0.0,"correct_comments":"","incorrect_comments":"","neutral_comments":"","correct_comments_html":"","incorrect_comments_html":"","neutral_comments_html":"","answers":[],"variables":null,"formulas":null,"answer_tolerance":null,"formula_decimal_places":null,"matches":null,"matching_answer_incorrect_matches":null}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if question, err := api.CreateQuestion(666, 777, &params); err != nil {
		t.Fatal("Error creating question", err)
	} else {
		require.Equal(t, question.Id, CanvasId(14102))
		require.Equal(t, question.QuizId, CanvasId(1652))
		require.Equal(t, question.QuestionName, "Floop")
		require.Equal(t, question.QuestionText, "cyber")
	}
}

func TestAddQuestionGroup(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.Method, "POST")
			require.Equal(t, req.URL.Path, "/api/v1/courses/123/quizzes/456/groups")
			if value, ok := req.URL.Query()["quiz_groups[][name]"]; !ok {
				t.Fatal("Did not receive a quiz name")
			} else {
				require.Equal(t, value[0], "A Name")
			}
			if value, ok := req.URL.Query()["quiz_groups[][pick_count]"]; !ok {
				t.Fatal("Did not receive a pick count")
			} else {
				require.Equal(t, value[0], "2")
			}
			if value, ok := req.URL.Query()["quiz_groups[][question_points]"]; !ok {
				t.Fatal("Did not receive a question count")
			} else {
				require.Equal(t, value[0], "3.20")
			}
			w.WriteHeader(http.StatusCreated) // here Canvas sends a 201 instead of a 200...
			w.Write([]byte(`{"quiz_groups":[{"id":643,"quiz_id":1660,"name":"Gruppenbla","pick_count":1,"question_points":1.0,"position":1,"assessment_question_bank_id":null}]}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if group, err := api.CreateQuizQuestionGroup(123, 456, "A Name", 2, 3.2); err != nil {
		t.Fatal("Error creating question", err)
	} else {
		require.Equal(t, group.Id, CanvasId(643))
		require.Equal(t, group.QuizId, CanvasId(1660))
		require.Equal(t, group.Name, "Gruppenbla")
		require.Equal(t, group.PickCount, 1)
		require.Equal(t, group.QuestionPoints, float64(1.0))
	}
}
