/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func (a *ApiImpl) ListQuizzes(id CanvasId) ([]Quiz, error) {
	logrus.Debugf("Listing quizzes for course %d", id)
	uri := a.makeUri("courses", id.ToBase10String(), "quizzes").String()
	req, err := a.assembleRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}
	//TODO: Pagination
	var response []Quiz
	err = a.processRequest(req, &response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (a *ApiImpl) CreateQuiz(
	courseId CanvasId,
	title,
	quizType,
	text,
	hideResults,
	showCorrectAnswersAt,
	hideCorrectAnswersAt,
	scoringPolicy,
	accessCode,
	IPFilter,
	dueAt,
	lockAt,
	unlockAt *string,
	timeLimit,
	allowedAttempts *int,
	shuffleAnswers,
	showCorrectAnswers,
	showCorrectAnswersLastAttempt,
	oneQuestionAtATime,
	cantGoBack,
	published,
	oneTimeResults *bool,
	) (Quiz, error) {
	logrus.Debugf("Creating a quiz for course %d", courseId)

	quizSpecPtrString := map[string]*string{
		"quiz[title]":       						title,
		"quiz[quiz_type]":   						quizType,
		"quiz[description]": 						text,
		"quiz[hide_results]":						hideResults,
		"quiz[show_correct_answers_at]":			showCorrectAnswersAt,
		"quiz[hide_correct_answers_at]":			hideCorrectAnswersAt,
		"quiz[scoring_policy]":						scoringPolicy,
		"quiz[access_code]":						accessCode,
		"quiz[ip_filter]":							IPFilter,
		"quiz[due_at]":								dueAt,
		"quiz[lock_at]":							lockAt,
		"quiz[unlock_at]":							unlockAt,
	}

	quizSpecPtrInt := map[string]*int{
		"quiz[time_limit]":							timeLimit,
		"quiz[allowed_attempts]":					allowedAttempts,
	}

	quizSpecPtrBool := map[string]*bool{
		"quiz[shuffle_answers]": 					shuffleAnswers,
		"quiz[show_correct_answers":				showCorrectAnswers,
		"quiz[show_correct_answers_last_attempt":	showCorrectAnswersLastAttempt,
		"quiz[one_question_at_a_time]":				oneQuestionAtATime,
		"quiz[cant_go_back]":						cantGoBack,
		"quiz[published]":							published,
		"quiz[one_time_results]":					oneTimeResults,
	}

	quizSpecs := url.Values{}

	for queryAttribute, valuePtr := range quizSpecPtrString {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, *valuePtr)
		}
	}

	for queryAttribute, valuePtr := range quizSpecPtrInt {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, strconv.Itoa(*valuePtr))
		}
	}

	for queryAttribute, valuePtr := range quizSpecPtrBool {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, strconv.FormatBool(*valuePtr))
		}
	}

	uri := a.makeUri("courses", courseId.ToBase10String(), "quizzes").SetQuery(&quizSpecs).String()
	logrus.Debugf("RequestURI: %s", uri)
	req, err := a.assembleRequest("POST", uri, nil)
	if err != nil {
		return Quiz{}, err
	}
	var quiz Quiz
	err = a.processRequest(req, &quiz)
	if err != nil {
		return Quiz{}, err
	}
	return quiz, nil
}

type canvasQuestionGroupCreationResponse struct {
	Group []QuestionGroup `json:"quiz_groups"`
}

func (a *ApiImpl) CreateQuizQuestionGroup(courseId, quizId CanvasId, title string,
	pickCount int, questionPoints float64) (QuestionGroup, error) {

	logrus.Debugf("Creating a question group for course %d and quiz %d", courseId, quizId)
	uri := a.makeUri("courses", courseId.ToBase10String(),
		"quizzes", quizId.ToBase10String(), "groups").SetQuery(
		&url.Values{
			"quiz_groups[][name]":            []string{title},
			"quiz_groups[][pick_count]":      []string{strconv.Itoa(pickCount)},
			"quiz_groups[][question_points]": []string{strconv.FormatFloat(questionPoints, 'f', 2, 64)},
		}).String()
	logrus.Debugf("RequestURI: %s", uri)
	req, err := a.assembleRequest("POST", uri, nil)
	if err != nil {
		return QuestionGroup{}, err
	}
	// This one returns a 201 instead of a 200 on success
	respBytes, err := a.processRequestCheckRaw(req, []int{http.StatusCreated}, nil)

	var quiz canvasQuestionGroupCreationResponse
	if err = json.Unmarshal(respBytes, &quiz); err != nil {
		return QuestionGroup{}, err
	}
	if quiz.Group == nil || len(quiz.Group) < 1 {
		return QuestionGroup{}, fmt.Errorf("Error: Server responded with empty question group list")
	}
	return quiz.Group[0], nil
}

func (a *ApiImpl) CreateQuestion(courseId, quizId CanvasId, params *url.Values) (QuizQuestion, error) {
	logrus.Debugf("Adding a question to quiz %d (course %d)", quizId, courseId)
	logrus.Debugf("Params: %v", *params)
	uri := a.makeUri("courses", courseId.ToBase10String(),
		"quizzes", quizId.ToBase10String(), "questions").String()
	logrus.Debugf("RequestURI: %s", uri)
	req, err := a.assembleRequest("POST", uri, strings.NewReader(params.Encode()))
	if err != nil {
		return QuizQuestion{}, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	var question QuizQuestion
	err = a.processRequest(req, &question)
	if err != nil {
		return QuizQuestion{}, err
	}
	return question, nil
}
