/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/sirupsen/logrus"
	"net/url"
)

type FileModBuilder interface {
	Published(bool) FileModBuilder
	Build() map[string]string
}

type fileModBuilderImpl struct {
	spec map[string]string
}

func (b *fileModBuilderImpl) Published(publish bool) FileModBuilder {
	if b == nil {
		panic("Receiver was nil")
	}
	if publish {
		b.spec["locked"] = "false"
	} else {
		b.spec["locked"] = "true"
	}
	return b
}

func MakeFileModification() FileModBuilder {
	return &fileModBuilderImpl{spec: make(map[string]string)}
}

func (b *fileModBuilderImpl) Build() map[string]string {
	if b == nil {
		panic("Receiver was nil")
	}
	return b.spec
}

func (c *ApiImpl) ModifyFile(id CanvasId, mod FileModBuilder) (FileInfo, error) {
	query := url.Values{}
	spec := mod.Build()
	for key, value := range spec {
		query[key] = []string{value}
	}
	uri := c.makeUri("files", id.ToBase10String()).SetQuery(&query)
	logrus.Debugf("Modifying file with id %d. Modification %v", id, mod.Build())
	logrus.Debugf("This is the uri: %s", uri.String())
	req, err := c.assembleRequest("PUT", uri.String(), nil)
	if err != nil {
		return FileInfo{}, err
	}

	var finfo FileInfo
	err = c.processRequest(req, &finfo)
	if err != nil {
		return FileInfo{}, err
	}
	return finfo, nil
}
