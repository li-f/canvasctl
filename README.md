[![pipeline status](https://gitlab.com/mgelde/canvasctl/badges/master/pipeline.svg)](https://gitlab.com/mgelde/canvasctl/-/commits/master) [Coverage Report](https://gitlab.com/mgelde/canvasctl/-/jobs/artifacts/master/file/coverage.html?job=format)

# canvasctl: Controlling Canvas LMS from the command line


# General Usage:

See `canvasctl help` or `canvasctl help <command>` for a list of valid commands and flags.
Currently, there are commands to control aspects of the following features:
+ `accounts` to list the accounts associated with the current user
+ `courses` to list courses or show details about a given course
+ `files` to upload files and show information about files in a given course
+ `modules` to create and list modules in a course

Generally, all commands that operate on a specific course will need one of two optional parameters:
`-i` or `-n`. The first requires a numeric ID identifying the course (can be obtained using
`canvasctl courses list`), whereas the second accepts the human-readable name of the course:
```console
$ canvasctl courses list
name=My awesome course (id=1234)
...
$ canvasctl courses show -i 1234
Course:
--------
- Name: My awesome course 
- Id: 1234
- Code: MAC
- Started: 2020-02-02 02:02:02 +0100 CET
$ canvasctl courses show -n "My awesome course"
(same output)
```
Note that names containing white-spaces (i.e almost all of them) will usually require shell escapes
or quoting.


>>>
**Note**

It is entirely possible that the syntax of the command line will change slightly at this stage.
Please do not rely on the syntax being stable.

The same is true for the format of the output.
>>>

## Authentication

To use the tool, you need to make sure that it can authenticate towards canvas.
For this, we use a `canvas.conf` file. This file is in TOML syntax. An example
looks like this:
```toml
[Default]
URL = "https://your.canvas.url.com/"

[Sites]
[Sites.Site1]
URL = "https://your.canvas.url.com/"
Token = "enc1:salt(ElxAUE69wyMzoVR/wJxPz0iCOv1cT2eu6+/OgI5ymKs=):nonce(1mvIhV68U1bxyJxZqHDeZiTP8/wNj9Fh):qy1zwa7pJngPREGJ1/NPxU9S06sdfnOFubiiH0ujrdB+BPlpNbl8m5tcQZuOnGEQIA=="
```
We will get to the token shortly. The file must be in one of these locations,
which are searched in order:
+ `$XDG_CONFIG_HOME/canvasctl` (the default for `XDG_CONFIG_HOME` is `$HOME/.config`,
    so this is usually equivalent to the following item)
+ `$HOME/.config/canvasctl`
+ `$HOME/.cavasctl`
While these are Unix locations, they should work on Windows with Cygwin (not tested, though).

Please note that the URLs in the 
above example *must* match. The default URL simply allows you to invoke `canvasctl`
without having to specify the `--url` flag all the time. The `[Sites]` section is
where the important authentication info is stored. It holds a URL and an token
that `canvasctl` can use to authenticate against Canvas. These tokens are stored
in an encrypted way (see below).

To get a token, go to your account's settings pane and look for "Access Tokens".
You will be able to generate on. Give it an expriation date (seriously, please do),
and a name that will allow you to identify it (e.g. the name of the computer) you
wish to use it on.

**Note:** The token will only be displayed briefly, be sure to import it into 
`canvasctl` and verify that it works (as described below) *before* dismissing the
dialog. Do *not* store it elsewhere, as that defeats the purpose of encrypting it.
It's easy to generate a new one, so why take risks?

Now you can import the token:
```console
$ canvasctl encrypt-token https://your.canvas.url.com/
[+] Please provide the token:
Please provide a password:
Please repeat your password:
Place this in your canvas.conf file:
Token = "enc1:salt(ElxAUE69wyMzoVR/wJxPz0iCOv1cT2eu6+/OgI5ymKs=):nonce(1mvIhV68U1bxyJxZqHDeZiTP8/wNj9Fh):qy1zwa7pJngPREGJ1/NPxU9S06sdfnOFubiiH0ujrdB+BPlpNbl8m5tcQZuOnGEQIA=="
```
Verify that everything works:
```console
canvasctl courses show -i 1234
Please your decryption password:
Course:
--------
- Name: Bla bla bla
- Id: 1234
- Code: BLA
- Started: 2020-03-03 03:03:03 +0100 CET
```
Now you can dismiss that "new token" dialog.

**Note**  
This is not the ideal way (in terms of user-friendlyness) of doing this. I plan
on writing a keyring integration and/or using some kind of daemon to hold descrypted tokens
for some configurable time in memory (think ssh-agent). I simply did not have the
time yet.

**Crypto**  
In case you were wondering:
+ Argon2id for key generation with 32 byte salt (random), mem=64M, time=2
+ XChaCha20 with random nonce (24 bytes, random)
+ Poly1305 with authentication pass over the URL

Making the argon2id parameters depend on your machine is a useful improvement
for the future.

## Files

Files can be uploaded to a course using a command like this:
```console
$ canvasctl files -i 1234 push ./file1 ./file2 ./file3
- ./file1   [OK]
- ./file2   [OK]
- ./file3   [OK]
```
It is possible to automatically add files to a given module:
```console
$ canvasctl files -i 1234 push -m 'Random Variables' ./file1 ./file2 ./file3
```
**NOTE:** The module will be created, if it does not yet exist.

By default, Canvas publishes files uploaded in this way (the little green
checkmark next to the file in the Canvas web UI). This means that students will
be able to see the files right away. If this is not desired, `--publish=false`
can be passed as an option:
```console
$ canvasctl files -i 1234 push --publish=false ./file1
```
This is a bit of a hack, because it marks the file as "hidden". I did not see a
way to prevent Canvas from publishing it.

Finally, it is possible to set the destination path (i.e. the virtual folder
inside the Canvas course, in which the file will be placed) using `--path`. This
defaults to `--path /`, which is the root folder inside the course.

## Quizzes

A neat feature is the ability to create quizzes from TOML files. This means that quizzes (including
their answers) can be auto-generated using other means (e.g. sage math or similar) and then
automatically loaded into canvas. An example of such a TOML file can be found
[here](./examples/test.toml).

**Note:** The TOML file currently needs to be supplied via `STDIN`, like so:
```console
$ canvasctl quizzes -i 1234 create < ./examples/test.toml
# Or like this:
$ tool_that_generates_toml_quiz_on_stdout | canvasctl quizzes -i 1234 create
Creating quiz from STDIN
[+] Creating quiz: My Quiz
[+] Creating group: Group 1
 -> Creating MC question: Question 1
 -> Creating MC question: Question 2
 -> Creating multi-answer question: Question 3
[+] Creating group: Group 2
 -> Creating numeric question: Q1
```

## Known deficiencies
+ Not all question types are supported. More will be added eventually. Currently supported:
    + Numeric questions (answers can be given as range, as value + epsilon (float64), or as integer
      \+ integer margin)
    + Multiple choice questions (precisely one answer is correct)
    + Multiple answer questions (n out of m answers are correct)
+ Currently questions must be in a question-group, because this will allow canvas to select
    questions at random from those groups.

    If a single fixed question is needed, a group containing just one question is currently the only
    way to achieve that goal.
+ I do not support fancy HTML descriptions for the questions at this time.
