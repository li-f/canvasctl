/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package config

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/chacha20poly1305"
	"golang.org/x/crypto/ssh/terminal"
	"regexp"
	"strings"
	"syscall"
)

const (
	SaltSize       int = 32
	CRYPTO_VERSION int = 1
)

func decryptToken(ciphertext, nonce, salt, pwd []byte, url string) (string, error) {
	if len(nonce) != chacha20poly1305.NonceSizeX {
		return "", fmt.Errorf("Nonce has wrong size")
	}
	if len(salt) != SaltSize {
		return "", fmt.Errorf("Nonce has wrong size")
	}
	key := argon2.IDKey(pwd, salt, 2 /* time */, 64*1024 /*memory*/, 4, chacha20poly1305.KeySize)
	cipher, err := chacha20poly1305.NewX(key)
	if err != nil {
		return "", err
	}
	plaintextBytes := make([]byte, 0, len(ciphertext))
	plaintextBytes, err = cipher.Open(plaintextBytes, nonce, ciphertext, []byte(url))
	if err != nil {
		return "", err
	}
	return string(plaintextBytes), nil
}

type Ciphertext struct {
	Nonce      []byte
	Salt       []byte
	Ciphertext []byte
}

func EncryptToken(token, url string, pwd []byte) (Ciphertext, error) {
	salt := make([]byte, SaltSize)
	n, err := rand.Read(salt)
	if err != nil {
		return Ciphertext{}, err
	}
	if n != SaltSize {
		// according to the docs, if n < len(inArray), err should have been != nil
		// let's be a little paranoid anyway
		panic("salt too short")
	}
	// TODO: Paramers should be increased when machine provides support for more...
	key := argon2.IDKey(pwd, salt, 2 /* time */, 64*1024 /*memory*/, 4, chacha20poly1305.KeySize)

	// we use XChaCha20 so that random nonces are collision safe
	nonce := make([]byte, chacha20poly1305.NonceSizeX)
	n, err = rand.Read(nonce)
	if err != nil {
		return Ciphertext{}, err
	}
	if n != chacha20poly1305.NonceSizeX {
		// let's be a little paranoid anyway (see above)
		panic("Nonce too short")
	}
	cipher, err := chacha20poly1305.NewX(key)
	if err != nil {
		return Ciphertext{}, err
	}
	ciphertext := Ciphertext{}
	ciphertext.Ciphertext = make([]byte, 0, cipher.Overhead()+len(pwd))
	ciphertext.Ciphertext = cipher.Seal(ciphertext.Ciphertext, nonce, []byte(token) /* plaintext */, []byte(url) /* aad */)
	ciphertext.Salt = salt
	ciphertext.Nonce = nonce
	return ciphertext, nil
}
func EncodeToken(ciphertext, nonce, salt []byte) string {
	ciphB64 := base64.StdEncoding.EncodeToString(ciphertext)
	nonceB64 := base64.StdEncoding.EncodeToString(nonce)
	salt64 := base64.StdEncoding.EncodeToString(salt)
	encodedToken := fmt.Sprintf("enc%d:salt(%s):nonce(%s):%s", CRYPTO_VERSION, salt64, nonceB64, ciphB64)
	return encodedToken
}

func DecodeToken(encodedToken string) (Ciphertext, error) {
	if !IsEncrypted(encodedToken) {
		return Ciphertext{}, fmt.Errorf("Token does not appear to be encrypted")
	}

	exp, err := regexp.Compile(`enc(\d+):salt\(([^)]+)\):nonce\(([^)]+)\):(.+)`)
	if err != nil {
		return Ciphertext{}, err
	}
	match := exp.FindStringSubmatch(encodedToken)
	if len(match) != 5 {
		return Ciphertext{}, fmt.Errorf("Cannot parse encoded token")
	}
	if match[1] != fmt.Sprintf("%d", CRYPTO_VERSION) {
		return Ciphertext{}, fmt.Errorf("The version %s is not supported", match[1])
	}
	salt, err := base64.StdEncoding.DecodeString(match[2])
	if err != nil {
		return Ciphertext{}, fmt.Errorf("Cannot decode the salt: %s. Error: %v", match[2], err)
	}
	nonce, err := base64.StdEncoding.DecodeString(match[3])
	if err != nil {
		return Ciphertext{}, fmt.Errorf("Cannot decode the nonce: %s. Error: %v", match[3], err)
	}
	ciphertext, err := base64.StdEncoding.DecodeString(match[4])
	if err != nil {
		return Ciphertext{}, fmt.Errorf("Cannot decode the ciphertext: %s. Error: %v", match[4], err)
	}
	return Ciphertext{
		Nonce:      nonce,
		Salt:       salt,
		Ciphertext: ciphertext,
	}, nil
}

func IsEncrypted(token string) bool {
	return strings.HasPrefix(token, "enc")
}

func DecryptTokenForUrl(encodedToken, url string) (string, error) {
	ciphertext, err := DecodeToken(encodedToken)
	if err != nil {
		return "", err
	}
	fmt.Print("Please provide your decryption password: ")
	pwdBytes, err := terminal.ReadPassword(int(syscall.Stdin))
	fmt.Println()
	if err != nil {
		return "", err
	}
	pwdBytes = bytes.TrimSpace(pwdBytes)
	token, err := decryptToken(ciphertext.Ciphertext, ciphertext.Nonce, ciphertext.Salt, pwdBytes, url)
	if err != nil {
		return "", err
	}
	return token, nil
}
