/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package config

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strings"
)

type Site struct {
	Token string
	URL   string
}

type defaultSettings struct {
	URL string
}

type config struct {
	Default defaultSettings
	Sites   map[string]Site
}

type Config interface {
	GetTokenForURL(string) (string, error)
	GetDefaultURL() string
	SiteByUrl(url string) (Site, error)
}

func (c config) SiteByUrl(url string) (Site, error) {
	//TODO: Normalize URL?
	logrus.Debugf("Looking for site with URL '%s'", url)
	for key, site := range c.Sites {
		if site.URL == url {
			logrus.Debugf("Site %s matches", key)
			return site, nil
		}
		logrus.Debugf("Rejecting site '%s' with URL '%s'", key, site.URL)
	}
	return Site{}, fmt.Errorf("No site found")
}

const TokenFileBasename = "canvas.conf"

var configLocations [][]string = [][]string{
	{"$XDG_CONFIG_HOME", "canvasctl"},
	{"$HOME", ".config", "canvasctl"},
	{"$HOME", ".canvasctl"},
	// necessary for Windows
	{"$HOMEPATH", ".config", "canvasctl"},
	{"$HOMEPATH", ".canvasctl"},
}

func configFilePaths(locations [][]string) ([]string, error) {
	res := make([]string, 0, len(locations))

outer_loop:
	for _, tokens := range locations {
		logrus.Debugf("Looking at tokens %v", tokens)
		for index, token := range tokens {
			if strings.HasPrefix(token, "$") {
				resolved := os.Getenv(token[1:])
				if resolved == "" {
					//nothing to see here, move along sir
					logrus.Debugf("Could not resolve environment variable %s.", token)
					continue outer_loop
				}
				tokens[index] = resolved
			}
		}
		res = append(res, filepath.Join(filepath.Join(tokens...), TokenFileBasename))
	}
	if len(res) == 0 {
		return nil, fmt.Errorf("No paths were constructed.")
	}
	return res, nil
}

func exists(path string) bool {
	_, err := os.Stat(path)
	// we ignore other reasons stat might fail (e.g. perms),
	// because they will lead to the same end result
	return err == nil
}

func LoadConfig() (Config, error) {
	paths, err := configFilePaths(configLocations)
	if err != nil {
		return config{}, err
	}
	for _, path := range paths {
		if !exists(path) {
			continue
		}
		var context config
		if _, err := toml.DecodeFile(path, &context); err != nil {
			logrus.Warnf("Malformed config file at %s", path)
			logrus.Warn(err)
			continue
		}
		return context, nil
	}
	return config{}, fmt.Errorf("No valid config file found.")
}

func (c config) GetTokenForURL(url string) (string, error) {
	site, err := c.SiteByUrl(url)
	if err != nil {
		return "", fmt.Errorf("No token found for site %s", url)
	}
	return site.Token, nil
}

func (c config) GetDefaultURL() string {
	return c.Default.URL
}
