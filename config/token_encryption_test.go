/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package config

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestEncryption(t *testing.T) {
	const (
		token string = "imatoken"
		url   string = "https://example.com/bla/"
		pwd   string = "super secret"
	)
	encrypted, err := EncryptToken(token, url, []byte(pwd))
	require.Nil(t, err, "Encryption should succeed", err)

	decrypted, err := decryptToken(encrypted.Ciphertext, encrypted.Nonce, encrypted.Salt, []byte(pwd), url)
	require.Nil(t, err, "Decryption should not fail")
	require.Equal(t, token, decrypted)

	_, err = decryptToken(encrypted.Ciphertext, encrypted.Nonce, encrypted.Salt, []byte(pwd), url+" ")
	require.NotNil(t, err, "Decryption for different url should fail")
}

func TestIsEncrypted(t *testing.T) {
	const (
		valid   = "enc:bbalblablabla"
		invalid = "blablabla"
	)
	require.True(t, IsEncrypted(valid), "Starts with 'enc:', so should be valid")
	require.False(t, IsEncrypted(invalid), "Does not start with 'enc:', so should not be valid")
}

func TestEncodingDecoding(t *testing.T) {
	nonce := []byte("nonce")
	salt := []byte("salt")
	ciphertext := []byte("ciphertext")
	encoded := EncodeToken(ciphertext, nonce, salt)
	decoded, err := DecodeToken(encoded)
	require.Nil(t, err, "This is valid data, or so I thought:", encoded, "Got error:", err)
	require.Equal(t, "ciphertext", string(decoded.Ciphertext))
	require.Equal(t, "nonce", string(decoded.Nonce))
	require.Equal(t, "salt", string(decoded.Salt))
}
