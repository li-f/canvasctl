/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/canvas"
	"gitlab.com/mgelde/canvasctl/cmdline"
	"os"
)

func commandCourses(c *cli.Context) error {

	switch {
	case c.Command.Name == "list" || c.Command.Name == "courses" || c.Command.Name == "":
		context, err := canvas.NewContext(c)
		if err != nil {
			return cli.Exit(err, 1)
		}
		if err := context.ListCourses(); err != nil {
			cli.Exit(err, 1)
		}
	case c.Command.Name == "show":
		context, err := canvas.NewCourseContext(c)
		if err != nil {
			return cli.Exit(err, 1)
		}
		if err := context.ShowCourse(); err != nil {
			return cli.Exit(err, 1)
		}
	default:
		return cli.Exit(fmt.Sprintf("Command unknown '%s'.", c.Command.Name), 1)
	}
	return nil
}

func commandAccounts(c *cli.Context) error {
	ctx, err := canvas.NewContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	if err := ctx.ListAccounts(); err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}

func commandModules(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}

	if err := ctx.ListModules(); err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}

var commands = []*cli.Command{
	{
		Name:  "courses",
		Usage: "Manage courses",
		Subcommands: []*cli.Command{
			{
				Name:   "list",
				Action: commandCourses,
				Usage:  "List courses",
			},
			{
				Name:   "show",
				Action: commandCourses,
				Usage:  "Show details about a course",
				Flags: []cli.Flag{
					&cli.UintFlag{
						Name:    "id",
						Aliases: []string{"i"},
						Usage:   "Numeric ID of a course",
					},
					&cli.StringFlag{
						Name:    "name",
						Aliases: []string{"n"},
						Usage:   "Name of course",
					},
				},
			},
		},
	},
	{
		Name:   "accounts",
		Usage:  "Manage accounts",
		Action: commandAccounts,
	},
	{
		Name:   "modules",
		Usage:  "List and manipulate modules",
		Action: commandModules,
		Flags: []cli.Flag{
			&cli.UintFlag{
				Name:    "id",
				Aliases: []string{"i"},
				Usage:   "Numeric ID of a course",
			},
			&cli.StringFlag{
				Name:    "name",
				Aliases: []string{"n"},
				Usage:   "Name of course",
			},
		},
	},
	{
		Name:  "quizzes",
		Usage: "Manipulate quizzes",
		Flags: []cli.Flag{
			&cli.UintFlag{
				Name:    "id",
				Aliases: []string{"i"},
				Usage:   "Numeric ID of a course",
			},
			&cli.StringFlag{
				Name:    "name",
				Aliases: []string{"n"},
				Usage:   "Name of course",
			},
		},
		Subcommands: []*cli.Command{
			{
				Name:   "list",
				Action: cmdline.CommandQuizzesList,
				Usage:  "List quizzes",
			},
			{
				Name:   "create",
				Action: cmdline.CommandCreateQuizStdin,
				Usage:  "Create quiz from stdin",
			},
		},
	},
	{
		Name:   "encrypt-token",
		Usage:  "Encrypt a token. The output of this command must be place into you canvas.conf.",
		Action: cmdline.ImportToken,
	},
	{
		Name:  "files",
		Usage: "List and upload files",
		Flags: []cli.Flag{
			&cli.UintFlag{
				Name:    "id",
				Aliases: []string{"i"},
				Usage:   "Numeric ID of a course",
			},
			&cli.StringFlag{
				Name:    "name",
				Aliases: []string{"n"},
				Usage:   "Name of course",
			},
		},
		Subcommands: []*cli.Command{
			{
				Name:   "list",
				Action: cmdline.CommandFilesList,
				Usage:  "List files",
			},
			{
				Name:      "push",
				Action:    cmdline.CommandFilePush,
				Usage:     "Push a file",
				ArgsUsage: "FILE [FILE...]",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "module",
						Aliases: []string{"m"},
						Usage:   "Place in named module (create if necessary)",
						Value:   "",
					},
					&cli.StringFlag{
						Name:  "path",
						Usage: "Folder to upload file to (create if necessary)",
						Value: "/",
					},
					&cli.BoolFlag{
						Name:  "publish",
						Usage: "Whether to publish the file after upload",
						Value: true,
					},
					&cli.BoolFlag{
						Name:  "details",
						Usage: "Whether to show the file URL after upload",
						Value: false,
					},
				},
			},
		},
	},
}

var globalFlags = []cli.Flag{
	&cli.StringFlag{
		Name:    "url",
		Aliases: []string{"u"},
		Usage:   "The url to connect to",
	},
	&cli.StringFlag{
		Name:  "log",
		Usage: "Set log output. Filename (will be truncated), 'stderr', or  empty (do not log).",
		Value: "",
	},
	&cli.StringFlag{
		Name:  "log-level",
		Usage: "Loglevel. Must be int between 0 and 5 (including).",
		Value: "error",
	},
}

func setUpLogging(c *cli.Context) error {
	if level, err := logrus.ParseLevel(c.String("log-level")); err != nil {
		logrus.SetLevel(logrus.ErrorLevel)
		logrus.Error("Level not recongnized:", level)
	} else {
		logrus.SetLevel(level)
	}

	//default is stderr, so do nothing if that is specified.
	if c.IsSet("log") && c.String("log") != "stderr" {
		fileHandle, err := os.Open(c.String("log"))
		if err != nil {
			return cli.Exit(err, 1)
		}
		logrus.SetOutput(bufio.NewWriter(fileHandle))
	}
	formatter := logrus.TextFormatter{}
	formatter.DisableColors = true
	formatter.FullTimestamp = true
	formatter.FullTimestamp = true
	logrus.SetFormatter(&formatter)
	return nil
}

func main() {
	app := cli.NewApp()
	app.Usage = "Control Canvas from the command line"
	app.Commands = commands
	app.Flags = globalFlags

	app.Before = func(c *cli.Context) error {
		err := setUpLogging(c)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error setting up logging", err)
			return err
		}
		return nil
	}
	app.Run(os.Args)
}
