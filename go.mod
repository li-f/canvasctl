module gitlab.com/mgelde/canvasctl

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.22.4 // indirect
	github.com/urfave/cli/v2 v2.1.1
	golang.org/x/crypto v0.0.0-20200403201458-baeed622b8d8
)
